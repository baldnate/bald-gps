package com.github.baldnate.baldgps;

import android.app.Service;
import android.content.Intent;
import android.location.GpsStatus.NmeaListener;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public class BaldGPSService extends Service {
    private LocationListener m_ll;
    private LocationManager m_lm;
    private String m_accuracy = "";
    private String m_altitude = "";
    private String m_fixTime = "";
    private String m_fixType = "";
    private String m_hdop = "";
    private String m_latitude = "";
    private String m_longitude = "";
    private String m_numOfGLONASSSatsInView = "";    
    private String m_numOfGPSSatsInView = "";
    private String m_numOfSatsInSolution = "";
    private String m_pdop = "";
    private String m_speed = "";
    private String m_track = "";
    private String m_vdop = "";

    public String getDetailedStatusString() {
        return 
            "Fix time: \t" + m_fixTime + "\n" +
            "Lat: \t" + m_latitude + "\n" +
            "Long: \t" + m_longitude + "\n" +
            "Altitude: \t" + m_altitude + "\n" +
            "Speed: \t" + m_speed + "\n" +
            "Track: \t" + m_track + "\n" +
            "GPS sats in view: \t" + m_numOfGPSSatsInView + "\n" +
            "GLONASS sats in view: \t" + m_numOfGLONASSSatsInView + "\n" +
            "Sats used in solution: \t" + m_numOfSatsInSolution + "\n" +
            "PDOP/HDOP/VDOP: \t" + m_pdop + "/" + m_hdop + "/" + m_vdop + "\n" +
            "Accuracy:\t" + m_accuracy + "\n" + 
            "Fix mode: \t" + m_fixType + "\n";
    }

    private GpsStatus.NmeaListener nl = new GpsStatus.NmeaListener() {
        @Override
        public void onNmeaReceived(long timestamp, String nmea) {
            String splits[] = nmea.split(",");
            String type = splits[0];
            if (Pattern.matches("\\$GPGSV", type)) {
                // $GLGSV,3,1,SATS IN VIEW,77,35,324,25,76,38,036,28,87,35,142,25,67,34,285,24*69
                m_numOfGPSSatsInView = splits[3];
            } else if (Pattern.matches("\\$GLGSV", type)) {
                // $GLGSV,3,1,SATS IN VIEW,77,35,324,25,76,38,036,28,87,35,142,25,67,34,285,24*69
                m_numOfGLONASSSatsInView = splits[3];
            } else if (Pattern.matches("\\$..GNS", type)) {
                // $GNGNS,HHMMSS.SS,DDMM.SSSSSS,N,09745.818197,W,modes,NUM OF SOLUTION SATS,HDOP,ALT,GEOID SEP,,*54
                m_numOfSatsInSolution = splits[7];
                m_hdop = splits[8];
            } else if (Pattern.matches("\\$..GSA", type)) {
                // $GNGSA,A,FIX LEVEL,03,06,07,08,10,13,16,19,23,,,,PDOP,HDOP,VDOP*29
                if (splits[2].equals("1")) {
                    m_fixType = "Fix not available";
                } else if (splits[2].equals("2")) {
                    m_fixType = "2D";
                } else if (splits[2].equals("3")) {
                    m_fixType = "3D";
                } else {
                    m_fixType = "Unknown";
                }
                m_pdop = splits[15];
                m_hdop = splits[16];
                m_vdop = splits[17].split("\\*")[0];
            } else if (Pattern.matches("\\$..GGA", type)) {
                // $GPGGA,HHMMSS.SS,DDMM.MMMMM,NS,DDDMM.MMMMM,EW,QUALITY,SATS IN SOLUTION,HDOP,ALT,M(eters),GEOID SEP,M,,*6C
                m_numOfSatsInSolution = splits[7];
                m_hdop = splits[8];
            } else if (Pattern.matches("\\$..RMC", type)) {
                // $GPRMC,HHMMSS.SS,A,   DDMM.MMM,N,   DDDMM.MMM,W,knots,track made good,DDMMYY,mag var,V,M,NS*CC
            } else if (Pattern.matches("\\$..VTG", type)) {
                // $GPVTG,true course,T,mag course,M,speed in knots,N,speed in km/h,K,MODE*23
            } else if (Pattern.matches("\\$..XFI", type)) {
                // $PQXFI,063825.0,  3025.257724,N,09745.818199,W,261.7,9.91,4.07,1.37*78
                // $P* is for vendor specific messages.  I can't find anything about this on the net, though.
            } else {
                Log.v("BaldGPSService/NMEA", "Unexpected NMEA sentence:");
                Log.v("BaldGPSService/NMEA", nmea);
            }
        }
    };

    public BaldGPSService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        m_ll = new LocationListener() {
            public void onLocationChanged(Location loc) {
                //sets and displays the lat/long when a location is provided
                m_latitude = loc.convert(loc.getLatitude(), loc.FORMAT_SECONDS);
                m_longitude = loc.convert(loc.getLongitude(), loc.FORMAT_SECONDS);
                m_altitude = Double.toString(loc.getAltitude());
                m_track = Double.toString(loc.getBearing());                
                m_speed = Double.toString(loc.getSpeed());
                m_accuracy = Double.toString(loc.getAccuracy());

                SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ssaa");
                Date fixDate = new Date(
                    System.currentTimeMillis() - (SystemClock.elapsedRealtimeNanos() / 1000000) + (loc.getElapsedRealtimeNanos() / 1000000)
                );
                m_fixTime = dateFormat.format(fixDate);
            }
             
            public void onProviderDisabled(String provider) {}
            public void onProviderEnabled(String provider) {}
            public void onStatusChanged(String provider, int status, Bundle extras) {}
        };

        m_lm = (LocationManager) getSystemService(getApplicationContext().LOCATION_SERVICE);
        m_lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, m_ll);
        m_lm.addNmeaListener(nl);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, R.string.service_started, Toast.LENGTH_SHORT).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, R.string.service_stopped, Toast.LENGTH_SHORT).show();
    }

    public class LocalBinder extends Binder {
        BaldGPSService getService() {
            return BaldGPSService.this;
        }
    }

    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
}

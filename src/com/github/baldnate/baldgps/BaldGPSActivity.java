package com.github.baldnate.baldgps;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.GpsStatus.NmeaListener;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.TextView;
import com.github.baldnate.baldgps.BaldGPSService;
import java.util.Calendar;
import java.util.regex.Pattern;

public class BaldGPSActivity extends Activity {

    private BaldGPSService mBoundService;

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mBoundService = ((BaldGPSService.LocalBinder)service).getService();
            mIsBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            mBoundService = null;
            mIsBound = false;
        }
    };

    private boolean mIsBound = false;
    void doBindService() {
        bindService(new Intent(BaldGPSActivity.this, 
                BaldGPSService.class), mConnection, getApplicationContext().BIND_AUTO_CREATE);
        
    }

    void doUnbindService() {
        if (mIsBound) {
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        m_handler.removeCallbacks(m_run);
        doUnbindService();
    }
    
    private TextView mTextView;
    private void updateString() {
        if (mIsBound) mTextView.setText(mBoundService.getDetailedStatusString());
    }

   Handler m_handler = new Handler();
   Runnable m_run = new Runnable() {
        @Override
        public void run() {
           updateString();
           m_handler.postDelayed(this, 250);
        }
    };


    public BaldGPSActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doBindService();
        setContentView(R.layout.bald_gps_activity);
        mTextView = (TextView) findViewById(R.id.textview);
        m_handler.postDelayed(m_run, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
